from pathlib import Path
import os

Path.with_stem   = lambda self,x: self.with_name(f'{x}{self.suffix}')
Path.append_stem =  lambda self,x :  self.with_stem(f'{self.stem}{x}')

def appendName(FileOrPath,strToAppend):
    pathCurr = Path(FileOrPath)
    pathCurr.with_stem(strToAppend)
    return pathCurr

def renameAppend(filePath,strToAppend):
    filePath.rename(filePath.append_stem(strToAppend).as_posix())

def renameRTruncate(filePath,strToTruncate):
    filePath.rename(filePath.with_stem(filePath.stem.rstrip(strToTruncate)))

def renameAppendGlob(dirName,globStr,strToAppend):
    for cur in Path(dirName).glob(globStr):
        renameAppend(cur,strToAppend)

def renameRTruncateGlob(dirName,globStr,strToTruncate):
    for cur in Path(dirName).glob(globStr):
        renameRTruncate(cur,strToTruncate)


Path.renameAppend        = lambda self,x : renameAppend(self,x)

Path.appendName          = lambda self,x : appendName(self,x)

Path.renameAppendGlob    = lambda self,globStr,strToAppend: renameAppendGlob(self,globStr,strToAppend)

Path.renameRTruncateGlob = lambda self,globStr,strToAppend: renameRTruncateGlob(self,globStr,strToAppend)


def test_fs():
    os.chdir(r'c:\temp\rename')
    #Path('Hammer_3_1_18.csv').renameAppend('_abc')
    #Path('.').renameAppendGlob('*.csv','_cde')
    Path('.').renameRTruncateGlob('*.csv','_cde')

#if __name__ == "__main__":
    